const jwt = require('jsonwebtoken')
const secret = require('../SecretWord')
exports.getToken = (req) => {
    const authorization = req.get('Authorization')
    let token = '';
    let res = null;
    if (authorization && authorization.toLowerCase().startsWith('bearer')) {
        token = authorization.substring(7)
    }
    console.log(token)
    const decodedToken = {};
    try {
        decodedToken = jwt.verify(token, secret.Word)
    } catch (e) {
        console.log(e)
    }

    console.log(decodedToken)
    if (!token || !decodedToken.rol) {
        res = false;
    }
    else {
        res = true;
    }
    console.log(res);
    return res;
}