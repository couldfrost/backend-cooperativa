const db= require("./DBConection") ;
const Claims= db.Claim;
const Partners=db.Partner;
const Meters=db.Meter;
const Inspections=db.Inspection;
const WorkDone=db.WorkDone;
const ListMaterial=db.ListMaterial;

//uno a muchos
//Partners.hasMany(Claims,{as:"Socios", foreignKey:"partner_id" });
//Claims.belongsTo(Partners,{as:"reclamo"});
Claims.belongsTo(Partners, {foreignKey: 'partner_id',targetKey:'partner_id', as: 'socio' });
Partners.hasMany(Claims,{foreignKey: 'partner_id',targetKey:'claims_id',as:"reclamos"});

Meters.belongsTo(Partners, {foreignKey: 'partner_id',targetKey:'partner_id', as: 'socio' });
Partners.hasMany(Meters,{foreignKey: 'partner_id',targetKey:'meter_id',as:"medidores"});

Inspections.belongsTo(Claims,{foreignKey: 'claims_id', targetKey:'claims_id', as: 'reclamo'});
Claims.hasOne(Inspections,{foreignKey:'claims_id',targetKey:'inspections_id',as:'inspeccion'});

WorkDone.belongsTo(Inspections,{foreignKey: 'inspections_id', targetKey:'inspections_id', as: 'inspeccion'});
Inspections.hasOne(WorkDone,{foreignKey:'inspections_id',targetKey:'work_done_id',as:'trabajo_realizado'});
WorkDone.belongsTo(Claims,{foreignKey: 'claims_id', targetKey:'claims_id', as: 'reclamo'});
Claims.hasOne(WorkDone,{foreignKey:'claims_id',targetKey:'work_done_id',as:'trabajo_realizado'});

ListMaterial.belongsTo(WorkDone, {foreignKey: 'work_done_id',targetKey:'work_done_id', as: 'trabajo_realizado' });
WorkDone.hasMany(ListMaterial,{foreignKey: 'work_done_id',targetKey:'list_material_id',as:"lista_materiales"});
