
const Sequelize = require('sequelize');
const MysqlDBConfig= require('../../MysqlDBConfig');
const sequelize=new Sequelize(MysqlDBConfig.DATABASE,MysqlDBConfig.USER,MysqlDBConfig.PASSWORD,{
    host: MysqlDBConfig.HOST,
    dialect: MysqlDBConfig.dialect});

const db={};
db.Sequelize=Sequelize;
db.sequelize=sequelize;

db.Claim=require('../Models/claimModel')(sequelize,Sequelize);
db.Partner=require('../Models/partnerModel')(sequelize,Sequelize);
db.User=require('../Models/UserModel')(sequelize,Sequelize);
db.Meter=require('../Models/meterModel')(sequelize,Sequelize);
db.Inspection=require('../Models/inspectionModel')(sequelize,Sequelize);
db.Material=require('../Models/materialModel')(sequelize,Sequelize);
db.ListMaterial=require('../Models/listMaterialModel')(sequelize,Sequelize);
db.WorkDone=require('../Models/workDoneModel')(sequelize,Sequelize);

module.exports = db;