module.exports=(sequelize,Sequelize)=>{
    const User = sequelize.define('users',{
    "user_id":{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true},
    "name":{type:Sequelize.STRING,allowNull: false},
    "email":{type:Sequelize.STRING,allowNull: false},
    "password":{type:Sequelize.TEXT,allowNull: false},
    "rol":{type:Sequelize.STRING,allowNull: false}
    },{
        timestamps: false
    });
    return User;
};