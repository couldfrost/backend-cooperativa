module.exports=(sequelize,Sequelize)=>{
    const Material = sequelize.define('materials',{
    "material_id":{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true,unique:true},
    "material_name":{type:Sequelize.STRING,allowNull: false}
    },{
        timestamps: false
    });
    
    return Material;
};