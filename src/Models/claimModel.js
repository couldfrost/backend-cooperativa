module.exports=(sequelize,Sequelize)=>{
    const Claim = sequelize.define('claims',{
    "claims_id":{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true,unique:true},
    "claims_date":{type:Sequelize.DATEONLY,allowNull: false},
    "phone":{type:Sequelize.INTEGER},
    "meter_number":{type:Sequelize.INTEGER},
    "direction":{type:Sequelize.STRING,allowNull: false},
    "descriptions":{type:Sequelize.STRING,allowNull: false},
    "states":{type:Sequelize.STRING,allowNull: false},
    "comment":Sequelize.STRING,
    "lat":{type:Sequelize.DOUBLE,allowNull: false},
    "lng":{type:Sequelize.DOUBLE,allowNull: false}
    },{
        timestamps: false
    });
    
    return Claim;
};