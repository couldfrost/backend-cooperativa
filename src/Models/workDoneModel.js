module.exports=(sequelize,Sequelize)=>{
    const WorkDone = sequelize.define('work_dones',{
    "work_done_id":{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true,unique:true},
    "date":{type:Sequelize.DATEONLY,allowNull: false},
    "description":{type:Sequelize.STRING,allowNull: false},   
    "repair_cost":{type:Sequelize.DECIMAL(10,2),allowNull: false},
    "stated":{type:Sequelize.STRING,allowNull: false}
    },{
        timestamps: false
    });
    
    return WorkDone;
};