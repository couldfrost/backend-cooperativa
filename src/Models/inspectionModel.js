module.exports=(sequelize,Sequelize)=>{
    const Inspection = sequelize.define('inspections',{
    "inspections_id":{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true,unique:true},
    "name_plumber":{type:Sequelize.STRING,allowNull: false},
    "description":{type:Sequelize.STRING,allowNull: false},
    "inspections_date":{type:Sequelize.DATEONLY,allowNull: false},
    
    "name_pay":Sequelize.STRING
    },{
        timestamps: false
    });
    
    return Inspection;
};