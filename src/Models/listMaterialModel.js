module.exports=(sequelize,Sequelize)=>{
    const ListMaterial = sequelize.define('list_materials',{
    "list_material_id":{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true,unique:true},
    "article":{type:Sequelize.STRING,allowNull: false},
    "amount":{type:Sequelize.DECIMAL(10,2),allowNull: false},
    "observation":Sequelize.STRING
    },{
        timestamps: false
    });
    
    return ListMaterial;
};