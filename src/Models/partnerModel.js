module.exports=(sequelize,Sequelize)=>{
    const Partner = sequelize.define('partners',{
    "partner_id":{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true,unique:true},
    "ci":{type:Sequelize.INTEGER,allowNull: false,unique:true},
    "partner_name":{type:Sequelize.STRING,allowNull: false},
    "surname_father":{type:Sequelize.STRING,allowNull: false},
    "surname_mother":{type:Sequelize.STRING,allowNull: false},
    "date_birth":{type:Sequelize.DATEONLY,allowNull: false},
    "direction":{type:Sequelize.STRING,allowNull: false},
    "phone":{type:Sequelize.INTEGER,allowNull: false},    
    "stated":{type:Sequelize.STRING,allowNull: false}

    
    },{
        timestamps: false
    });
    return Partner;
};