module.exports=(sequelize,Sequelize)=>{
    const Meter = sequelize.define('meters',{
        "meter_id":{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true,unique:true},
        "number_meter":{type:Sequelize.INTEGER,allowNull: false},
        "direction_meter":{type:Sequelize.STRING,allowNull: false},
        "date_registered":{type:Sequelize.DATEONLY,allowNull: false}
        
        },{
            timestamps: false
        });

    return Meter
};