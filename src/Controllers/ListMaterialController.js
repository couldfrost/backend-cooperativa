const express = require('express');
const db = require('../database/DBConection');
const ListMaterial = db.ListMaterial;
//const WorkDone = db.WorkDone;
const modelRequestListMaterial = require('../ModelRequest/modelRequestListMaterial');
const router = express.Router();

router.post("", async function (req, res) {

    
    console.log(req);
    ListMaterial.create(modelRequestListMaterial.getRequest(req)).then(data => {
       res.send(data);

    }).catch(err => {
       //console.log(err)
       res.status(500).send({
          message: err.message || "a ocurrido un error al crear un reclamo"

       });

    });

 })
 router.get("",async function(req,res){
      
    ListMaterial.findAll()
    .then(listMaterials=>{
      res.json(listMaterials);
      //console.log(res.json(listMaterials))
       })
    .catch(err=>{
       res.status(500).send({
          message:
                 err.message||"some error occurred while retrieving claims"
       });
       
    });
   
    
 });

 router.get("/:id", async function(req,res){
   let id=req.params.id;
   ListMaterial.findAll({
      where:{
         list_material_id: id
      }
      
   }).then(list_material=> res.json(list_material) );

   //console.log(partner===null)
});
module.exports = router;
