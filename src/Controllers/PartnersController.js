const express = require('express');
//const { Claim } = require('../database/DBConection');
const db = require('../database/DBConection');
const Partner = db.Partner;
const Meter = db.Meter;
const Claim = db.Claim;
const modelRequestPartner = require('../ModelRequest/modelRequestPartner');
//const { route } = require('./ClaimsController');
const router = express.Router();

router.post("/ci", async function (req, res) {
   let Partnerci = req.body.ci;
   const partner = await Partner.findAll({
      where: {
         ci: Partnerci
      },
      include: { model: Meter, as: "medidores" }

   });
   //console.log(partner);
   res.json(partner);
});
router.post("", async function (req, res) {


   const partner= await Partner.findOne({
      where: { ci: req.body.ci }
   })
   //console.log(res);
   //console.log(req.body.claims_date);
   //console.log(claim);
   //const claim= new modelRequestClaim;
   if (!partner) {
      Partner.create(modelRequestPartner.getRequest(req)).then(data => {
         res.send(data);

      }).catch(err => {
         //console.log(err)
         res.status(500).send({
            message: err.message || "a ocurrido un error al crear un reclamo"

         });

      });
   }
   else { res.json() }

});
router.get("", async function (req, res) {

   Partner.findAll({ include: { model: Meter, as: "medidores" },order: ["partner_id"] })
      .then(partners => {
         res.json(partners);
         //console.log(res.json(claims))
      })
      .catch(err => {
         res.status(500).send({
            message:
               err.message || "some error occurred while retrieving claims"
         });

      });


});

router.get("/:id", async function (req, res) {
   let id = req.params.id;
   Partner.findAll({
      where: {
         partner_id: id
      }

   }).then(partner => res.json(partner));

   //console.log(partner===null)
});
router.delete("/:id", async function (req, res) {
   let id = req.params.id;
   //console.log(id)
   Partner.destroy({
      where: {
         partner_id: id
      }
   }).then(() => res.send("partner delete"))

});
router.put("/:id", async function (req, res) {
   Partner.update({
      stated: req.body.stated
   },
      { where: { partner_id: req.params.id } }).then(result => { res.json(result); }).catch(err => {
         res.status(500).send({
            message:
               err.message || "some error occurred while retrieving claims"
         });

      })
})
module.exports = router;