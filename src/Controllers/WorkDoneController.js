const express = require('express');
const db = require('../database/DBConection');
const WorkDone = db.WorkDone;
const ListMaterial = db.ListMaterial;
const Inspection = db.Inspection;
const Claim = db.Claim;
const modelRequestWorkDone = require('../ModelRequest/modelRequestWorkDone');
const router = express.Router();

router.get("/idInspection/:id", async function (req, res) {
   let idInspection = req.params.id;
   const workDone = await WorkDone.findOne({
      where: {
         inspections_id: idInspection
      },
      include: { model: ListMaterial, as: "lista_materiales" }

   });
   console.log(workDone);
   res.json(workDone);
});
router.post("", async function (req, res) {


   WorkDone.create(modelRequestWorkDone.getRequest(req)).then(data => {
      res.send(data);

   }).catch(err => {
      //console.log(err)
      res.status(500).send({
         message: err.message || "a ocurrido un error al crear un reclamo"

      });

   });

});



router.get("", async function (req, res) {

   WorkDone.findAll({ include: [{ model: ListMaterial, as: "lista_materiales" }, { model: Inspection, as: "inspeccion" }, { model: Claim, as: "reclamo" }] })
      .then(workDones => {
         res.json(workDones);
         //console.log(res.json(claims))
      })
      .catch(err => {
         res.status(500).send({
            message:
               err.message || "some error occurred while retrieving claims"
         });

      });


});

router.get("/:id", async function (req, res) {
   let id = req.params.id;
   WorkDone.findOne({
      where: {
         work_done_id: id
      },
      include: [{ model: ListMaterial, as: "lista_materiales" }, { model: Inspection, as: "inspeccion" }, { model: Claim, as: "reclamo" }] 


   }).then(work_done => res.json(work_done));

   //console.log(partner===null)
});

router.put("/:id", async function (req, res) {
   console.log(req.params.id)
   WorkDone.update({
      stated: req.body.stated,
      repair_cost: req.body.repair_cost
   },
      { where: { work_done_id: req.params.id } }).then(result => { res.json(result); }).catch(err => {
         res.status(500).send({
            message:
               err.message || "some error occurred while retrieving claims"
         });

      })
})
module.exports = router;