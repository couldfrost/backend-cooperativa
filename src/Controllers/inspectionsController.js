const express = require('express');
const db = require('../database/DBConection');
const Claim = db.Claim;
const Inspection = db.Inspection;
const WorkDone = db.WorkDone;
const modelRequestInspection= require('../ModelRequest/modelRequestInspection')
const router = express.Router();



   router.post("", async function (req, res) {

      Inspection.create(modelRequestInspection.getRequest(req)).then(data => {
         res.send(data);

      }).catch(err => {
         //console.log(err)
         res.status(500).send({
            message: err.message || "a ocurrido un error al crear un reclamo"

         });

      });

   })
   router.get("", async function (req, res) {
     
      Inspection.findAll({  include: [{ model: Claim, as: "reclamo" },{model: WorkDone, as: "trabajo_realizado" }] })
         .then(inspections => {
            res.json(inspections);
            //console.log(res.json(claims))
         })
         .catch(err => {
            res.status(500).send({
               message:
                  err.message || "some error occurred while retrieving claims"
            });

         })


   })
   module.exports = router;