const express = require('express');
const db = require('../database/DBConection');
const User = db.User;
const modelRequestUser = require('../ModelRequest/modelRequestUser');
const bcrypt = require('bcryptjs')
const router = express.Router();
const Op = db.sequelize;
router.post("", async function (req, res) {

   const email= await User.findOne({
      where: { email: req.body.email }
   })
   const name=await User.findOne({
      where: { name: req.body.name }
   })

   if(!name&&!email)
   {
   const hash = await bcrypt.hash(req.body.password, 10);
   req.body.password = hash;
   User.create(modelRequestUser.getRequest(req)).then(data => {
      res.send(data);

   }).catch(err => {

      res.status(500).send({
         message: err.message || "a ocurrido un error al crear un reclamo"

      });

   });
   }
   else{
      res.json();
   }

});
router.get("", async function (req, res) {

   User.findAll({ where: Op.or({rol:["SECRETARIA","PLOMERO"]}),
   attributes: ["rol","name", "email","user_id"] }).then(users => {
         res.json(users);
         //console.log(res.json(claims))
      })
      .catch(err => {
         res.status(500).send({
            message:
               err.message || "some error occurred while retrieving claims"
         });

      });


});

router.get("/:id", async function (req, res) {
   let id = req.params.id;
   const user = await User.findOne({

      where: {
         user_id: id
      },
      attributes: ["name", "email"]
   });
   console.log(res.json(user) == null);

   //console.log(partner===null)
})
router.delete("/:id", async function (req, res) {
   let id = req.params.id;
   //console.log(id)
   User.destroy({
      where: {
         user_id: id
      }

   }).then(() => res.send("user delete"))

});
router.put("", async function(req,res){
   //console.log(req.params.id)
   console.log(req.body.password)

   const hash = await bcrypt.hash(req.body.password, 10);
   console.log(hash);
   req.body.password = hash;
   
   console.log(req.body.password)
   User.update({password: req.body.password,
                name:req.body.name},
      {where: {email: req.body.email}}).then(result => {res.json(result);}).catch(err => {
         res.status(500).send({
            message:
               err.message || "some error occurred while retrieving claims"
         });

      })
})

router.put("/:id", async function (req, res) {
   //console.log(req.params.id);
   const hash = await bcrypt.hash(req.body.password, 10);
   req.body.password = hash;

   User.update({password: req.body.password},
      { where: { user_id: req.params.id } }).then(result => { res.json(result); }).catch(err => {
         res.status(500).send({
            message:
               err.message || "some error occurred while retrieving claims"
         });

      })
})
module.exports = router;