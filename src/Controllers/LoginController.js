const express = require('express');
const secret= require('../../SecretWord')
const db = require('../database/DBConection');
const User = db.User;
const jwt = require('jsonwebtoken')
//const modelRequestUser = require('../Models/ModelRequest/modelRequestUser');
const bcrypt = require('bcryptjs')
const router = express.Router();
//console.log(secret.Word)
router.post("", async function (req, res) {
   let UserEmail = req.body.email;
   let UserPass = req.body.password;
   //console.log(UserPass)
   const user = await User.findOne({
      where: {
         email: UserEmail
      }
   });
   console.log(user);


   if (user) {
      const validPass = await bcrypt.compare(UserPass, user.password)
      if (validPass) {
         const userForToken = {
            id: user.user_id,
            userEmail: user.email,
            rol: user.rol
         }
         //console.log(userForToken);
         const token = jwt.sign(userForToken, secret.Word);
         res.json({
            userEmail: user.email,
            rol: user.rol,
            token
         });
      }
      else {
         res.json();
         
      }
   }
   else {
      res.json();
      
   }



});
module.exports = router;