const express = require('express');
const db = require('../database/DBConection');
const Meter = db.Meter;
const Partner = db.Partner;
const modelRequestMeter = require('../ModelRequest/modelRequestMeter')
const router = express.Router();

router.post("", async function (req, res) {


   console.log(req);
   const meter = await Meter.findOne({
      where: { number_meter: req.body.number_meter, partner_id: req.body.partner_id }
   })
   console.log(meter)
   if (!meter) {
      Meter.create(modelRequestMeter.getRequest(req)).then(data => {
         res.send(data);

      }).catch(err => {
         //console.log(err)
         res.status(500).send({
            message: err.message || "a ocurrido un error al crear un reclamo"

         });

      });
   }
   else {
      res.json();
   }
   /*
   
*/
})
router.get("", async function (req, res) {

   Meter.findAll()
      .then(meters => {
         res.json(meters);
         //console.log(res.json(claims))
      })
      .catch(err => {
         res.status(500).send({
            message:
               err.message || "some error occurred while retrieving claims"
         });

      });


});

router.get("/:id", async function (req, res) {
   let id = req.params.id;
   Meter.findAll({
      where: {
         meter_id: id
      }

   }).then(meter => res.json(meter));

   //console.log(partner===null)
});
module.exports = router;
