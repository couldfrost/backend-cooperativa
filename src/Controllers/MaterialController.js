const express = require('express');
const db = require('../database/DBConection');
const Material = db.Material;
const modelRequestMaterial = require('../ModelRequest/modelRequestMaterial')
const router = express.Router();

router.post("", async function (req, res) {

   const name_material = await Material.findOne({
      where: { material_name: req.body.material_name }
   })
   if (!name_material) {
      console.log(req);
      Material.create(modelRequestMaterial.getRequest(req)).then(data => {
         res.send(data);

      }).catch(err => {
         //console.log(err)
         res.status(500).send({
            message: err.message || "a ocurrido un error al crear un reclamo"

         });

      });
   }
   else {
      res.json();
   }

})
router.get("", async function (req, res) {

   Material.findAll()
      .then(materials => {
         res.json(materials);
         //console.log(res.json(materials))
      })
      .catch(err => {
         res.status(500).send({
            message:
               err.message || "some error occurred while retrieving claims"
         });

      });


});

router.get("/:id", async function (req, res) {
   let id = req.params.id;
   Material.findAll({
      where: {
         material_id: id
      }

   }).then(material => res.json(material));

   //console.log(partner===null)
});
module.exports = router;
