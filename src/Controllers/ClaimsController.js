const express = require('express');
const db = require('../database/DBConection');
const Claim = db.Claim;
const Partner = db.Partner;
const Inspection = db.Inspection;
const WorkDone=db.WorkDone;
const modelRequestClaim = require('../ModelRequest/modelRequestClaim')
const router = express.Router();
const Op = db.sequelize;
const AuthMiddleware= require('../Middlewares/AuthMiddleware');

router.post("/maps", async function (req, res) {
   console.log(req)
   let month = req.body.month;
   //console.log(req.body);
   let year = req.body.year;
   Claim.findAll({

      where: {
         claims_date: db.sequelize.where(db.sequelize.fn('MONTH', db.sequelize.col('claims_date')), month),
         [Op.and]: db.sequelize.where(db.sequelize.fn('YEAR', db.sequelize.col('claims_date')), year)
      }

      , attributes: ["lat", "lng","descriptions","claims_id","claims_date"]
   }).then(claims => {
      res.json(claims);
   }).catch(err => {
      res.status(500).send({
         message:
            err.message || "some error occurred while retrieving claims"
      });

   }

   )
   //console.log(partner);

});

router.post("/history",AuthMiddleware, async function (req, res) {
   let month = req.body.month;
   //console.log(req.body);
   let year = req.body.year;
   Claim.findAll({

      where: {
         claims_date: db.sequelize.where(db.sequelize.fn('MONTH', db.sequelize.col('claims_date')), month),
         [Op.and]: db.sequelize.where(db.sequelize.fn('YEAR', db.sequelize.col('claims_date')), year)
      }

      , include:[{model:Partner,as:"socio"},{model:Inspection,as:"inspeccion"},{model:WorkDone,as:"trabajo_realizado"}]
   }).then(claims => {
      res.json(claims);
   }).catch(err => {
      res.status(500).send({
         message:
            err.message || "some error occurred while retrieving claims"
      });

   }

   )
   //console.log(partner);

});



router.post("/ci", async function (req, res) {
   /*
   const au=authetication.getToken(req)
   if(!au)
   {
       return res.status(401).json({error:'token invalido'})  
   }
   */
   let ClaimSearch = req.body.search;
   Claim.findAll({
      include: [{
         model: Partner,
         as: 'socio',
         where: {
            ci: ClaimSearch
         }
      }
         , { model: Inspection, as: "inspeccion" }],order:["claims_id"]

      //attributes: ["claims_id", "claims_date", "phone", "meter_number", "direction", "descriptions"], include: { model: Partner, as: "socio", attributes: ["partner_name"] }
   }).then(claims => {
      res.json(claims);
   }).catch(err => {
      res.status(500).send({
         message:
            err.message || "some error occurred while retrieving claims"
      });

   }

   )
   //console.log(partner);

});

router.post("/meter", async function (req, res) {
   let ClaimSearch = req.body.search;
   Claim.findAll({

      where: {
         meter_number: ClaimSearch
      }
      , include: [{ model: Inspection, as: "inspeccion" }, { model: Partner, as: "socio" }],order:["claims_id"]

      //attributes: ["claims_id", "claims_date", "phone", "meter_number", "direction", "descriptions"], include: { model: Partner, as: "socio", attributes: ["partner_name"] }
   }).then(claims => {
      res.json(claims);
   }).catch(err => {
      res.status(500).send({
         message:
            err.message || "some error occurred while retrieving claims"
      });

   }

   )
   //console.log(partner);

});

router.post("", async function (req, res) {

   exports.create = (req, res) => {
      if (!req.body.phone) {
         res.status(400).send({
            message: "no puede estar vacio"
         })
      }

   };

   //console.log(req.body.claims_date);
   //console.log(req);
   //const claim= new modelRequestClaim;
   Claim.create(modelRequestClaim.getRequest(req)).then(data => {
      res.send(data);

   }).catch(err => {
      //console.log(err)
      res.status(500).send({
         message: err.message || "a ocurrido un error al crear un reclamo"

      });

   });

})
router.get("", async function (req, res) {
   //const claim = await Claim.findByPk(6);
   //const partner = await claim.getSocio({ raw: true });
   //console.log(partner);
   console.log("estamos aqui");
   const au=authetication.getToken(req)
   console.log(au)
   Claim.findAll({ include: [{ model: Partner, as: "socio" }, { model: Inspection, as: "inspeccion" },{model:WorkDone,as:"trabajo_realizado"}],order:["claims_id"] })
      .then(claims => {
         res.json(claims);
         //console.log(res.json(claims))
      })
      .catch(err => {
         res.status(500).send({
            message:
               err.message || "some error occurred while retrieving claims"
         });

      })

})
router.put("/:id", async function (req, res) {
   console.log(req.params.id)
   Claim.update({
      states: req.body.states,
      comment: req.body.comment
   },
      { where: { claims_id: req.params.id } }).then(result => { res.json(result); }).catch(err => {
         res.status(500).send({
            message:
               err.message || "some error occurred while retrieving claims"
         });

      })
})
module.exports = router;