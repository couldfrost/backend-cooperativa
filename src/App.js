const express = require('express')
//inicializa
const app = express();
const db = require('./database/DBConection');
require('./database/asociations')
const cors = require('cors');
var PORT = process.env.PORT || 3050;
app.use(cors());
app.use(express.json());
const path = require('path')

app.use(express.urlencoded({ extended: false }));
app.set("port", PORT);

(async function () {

    db.sequelize.authenticate().then(() => {
        console.log("connected");
    }).catch(err => {
        console.log("Error" + err);
    });

    //db.Claim.sequelize.sync();

    const roters = require('./routes');

    app.use(express.static(path.join(__dirname, 'build')));
    app.use('/api', roters);    
    app.use('*', function (req, res) {
        res.sendFile(path.join(__dirname, 'build', 'index.html'));
    });
    
    app.listen(PORT, function () {
        console.log('running on port' + PORT);
    })
})();


