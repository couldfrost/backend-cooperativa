const authetication=require('../decodedToken')
const AuthMiddleware = (req,res,next) => {
   const au=authetication.getToken(req)
   if(!au)
   {
       return res.status(401).json({error:'token invalido'})  
   }
   next();
}

module.exports = AuthMiddleware;