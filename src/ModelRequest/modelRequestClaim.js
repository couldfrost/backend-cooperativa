//class modelRequestClaim {
    exports.getRequest=(req)=> {
        const claim = {
            partner_id:req.body.partner_id,
            claims_date:req.body.claims_date,
            phone:req.body.phone,
            meter_number: req.body.meter_number,
            direction: req.body.direction,
            descriptions: req.body.descriptions,
            states: req.body.states,
            lat: req.body.lat,
            lng: req.body.lng
        };
        return claim;
    }
//}

//module.exports=modelRequestClaim;